// import logo from './logo.svg';
import "./App.css";
import { Component } from "react";

import CreatePlayer from "./page/createPlayer";
import UpdatePlayer from "./page/updatePlayer";
import SearchPlayer from "./page/searchPlayer";

class App extends Component {
  state = {
    activeMenu: "",
  };

  player = {
    username: "",
    password: "",
  };

  handleMenuChange = (event) => {
    this.setState({ activeMenu: event.target.id });
  };

  render() {
    return (
      <div className="App">
        <a id="createPlayer" onClick={this.handleMenuChange}>
          Create Player
        </a>
        <a id="updatePlayer" onClick={this.handleMenuChange}>
          Update Player
        </a>
        <a id="SearchPlayer" onClick={this.handleMenuChange}>
          Search Player
        </a>
        <br />
        {this.state.activeMenu == "createPlayer" && <CreatePlayer />}
        {this.state.activeMenu == "updatePlayer" && (
          <UpdatePlayer player={this.player} />
        )}
        {this.state.activeMenu == "SearchPlayer" && <SearchPlayer />}
      </div>
    );
  }
}

export default App;
