import Text from "../components/text";
import { Component } from "react";
import "../web.css";

class SearchPlayer extends Component {
  state = {
    username: "",
    showSummary: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showSummary: false,
    });
  };

  handleShowData = () => {
    this.setState({ showSummary: true });
  };

  render() {
    return (
      <>
        <h1>Form Search Player</h1>
        <Text
          fieldName="username"
          placeholder="Input your username"
          onChange={this.handleFieldChange}
          value={this.state.username}
        />
        <br></br>
        <Text
          fieldName="email"
          placeholder="Input your email"
          onChange={this.handleFieldChange}
          value={this.state.email}
        />
        <br></br>
        <Text
          fieldName="experience"
          placeholder="Input your experience"
          onChange={this.handleFieldChange}
          value={this.state.experience}
        />
        <br></br>
        <Text
          fieldName="lvl"
          placeholder="Input your lvl"
          onChange={this.handleFieldChange}
          value={this.state.lvl}
        />
        <br></br>
        <button onClick={this.handleShowData}>Search</button>
        <br />
        {this.state.showSummary && (
          <ul>
            <li>Username : {this.state.username}</li>
            <li>email : {this.state.email}</li>
            <li>experience : {this.state.experience}</li>
            <li>lvl : {this.state.lvl}</li>
          </ul>
        )}
      </>
    );
  }
}

export default SearchPlayer;
