import React, { Component } from "react";

function Text(props) {
  const { value, placeholder, fieldName, onChange } = props;

  return (
    <input
      type="text"
      onChange={onChange}
      name={fieldName}
      value={value}
      placeholder={placeholder}
    />
  );
}

export default Text;
